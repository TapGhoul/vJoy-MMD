#include "interpolation.h"
#include <math.h>

#ifndef FLT_EPSILON
#define FLT_EPSILON 1.192093e-07
#endif

float get_frame(const bone_key_t bk, const bone_axis_t axis)
{
	float w, x, y, z, gimbal_magnitude, output, yaw, roll;
	w = bk.w;
	x = bk.x;
	y = bk.y;
	z = bk.z;

	if (axis == YAW || axis == ROLL) {
		roll = atan2f(2.0f*(y*z + w*x), 1.0f - 2.0f * (x*x + y*y)); // X-axis
		yaw = atan2f(2.0f*(x*y + w*z), 1.0f - 2.0f * (y*y + z*z)); // Z-axis
	}

	// Check for gimbal lock
	if (fabs(x*z - w*y - 0.5f) < FLT_EPSILON) {
		if (axis == PITCH) {
			return -M_PI_2;
		} else {
			gimbal_magnitude = atan2f(x*y - w*z, x*z + w*y)/(roll+yaw);
			if (axis == YAW)
				return yaw * gimbal_magnitude;
			else
				return roll * gimbal_magnitude;
		}
	} else if (fabs(x*z - w*y + 0.5f) < FLT_EPSILON) {
		if (axis == PITCH) {
			return M_PI_2;
		} else {
			gimbal_magnitude = atan2f(x*y - w*z, x*z + w*y)/(roll-yaw);
			if (axis == YAW)
				return yaw * gimbal_magnitude;
			else
				return roll * gimbal_magnitude;
		}
	} else {
		switch (axis) {
			case PITCH:
				return asinf(-2.0f*(x*z - w*y));
			case YAW:
				return yaw;
			case ROLL:
				return roll;
		}

		// See http://www.sedris.org/wg8home/Documents/WG80485.pdf
		//bf.pitch = asinf(-2.0f*(x*z - w*y)); // Y-axis
		// Applied Yaw, Pitch, Roll - ZYX
	}

	//return bf;
}

bone_key_t bk_create(const vmdBoneKeyframe kf)
{
	bone_key_t bk;
	float magnitude = sqrtf(kf.rot_w*kf.rot_w + kf.rot_x*kf.rot_x + kf.rot_y*kf.rot_y + kf.rot_z*kf.rot_z);
	if (magnitude == 0.0f) {
		bk.w = 1.0f;
		bk.x = 0.0f;
		bk.y = 0.0f;
		bk.z = 0.0f;
	} else {
		bk.w = kf.rot_w / magnitude;
		bk.x = kf.rot_x / magnitude;
		bk.y = kf.rot_y / magnitude;
		bk.z = kf.rot_z / magnitude;
	}

	/*bk.w = kf.rot_w;
	bk.x = kf.rot_x;
	bk.y = kf.rot_y;
	bk.z = kf.rot_z;*/

	bk.frame_number = kf.frame_number;

	return bk;
}

float bk_interpolate(const uint32_t frame_number, const bone_key_t key1, const bone_key_t key2, const bone_axis_t axis)
{
	bone_key_t bk;
	float dist_between,
		omega, sin_omega, k1_omega, k2_omega;

	bk.frame_number = frame_number;

	if (frame_number <= key1.frame_number)
		return get_frame(key1, axis)/M_PI;
	else if (frame_number >= key2.frame_number)
		return get_frame(key2, axis)/M_PI;
	else
		dist_between = (float) (frame_number - key1.frame_number) / (float) (key2.frame_number - key1.frame_number);

	// Slerp
	omega = acosf(key1.w*key2.w + key1.x*key2.x + key1.y*key2.y + key1.z*key2.z);
	if (fabs(omega) > 1e-10f) {
		sin_omega = sinf(omega);
		k1_omega = sinf((1.0 - dist_between) * omega) / sin_omega;
		k2_omega = sinf(dist_between * omega) / sin_omega;

		bk.w = key1.w*k1_omega + key2.w*k2_omega;
		bk.x = key1.x*k1_omega + key2.x*k2_omega;
		bk.y = key1.y*k1_omega + key2.y*k2_omega;
		bk.z = key1.z*k1_omega + key2.z*k2_omega;
	} else {
		bk = key1;
	}

	/*bk.yaw = key1.yaw + (key2.yaw - key1.yaw)*dist_between;

	bk.pitch = key1.pitch + (key2.pitch - key1.pitch)*dist_between;

	bk.roll = key1.roll + (key2.roll - key1.roll)*dist_between;*/

	return get_frame(bk, axis)/M_PI;
}
