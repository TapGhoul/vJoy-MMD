#pragma once

#include "parser.h"
#include "frames.h"

typedef struct _pmd_bone_t_ {
	char bone_name[20];
	char english_bone_name[20];
	uint16_t parent_index, child_index;
	uint8_t bone_type;
	uint16_t target_bone;
	float x_pos, y_pos, z_pos;
} pmd_bone_t;

typedef struct _pmd_face_morph_t_ {
	char morph_name[20];
	char english_morph_name[20];
	uint32_t vertices;
	uint8_t face_type;
} pmd_face_morph_t;

/* Read a PMD file */
bone_store *pmd_read(const char *filename);
