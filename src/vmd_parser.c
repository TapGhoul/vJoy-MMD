#include "vmd_parser.h"
#include "pmd_parser.h"
#include "hashtable.h"
#include "interpolation.h"
#include <stdlib.h>

vmdBoneKeyframe vmd_read_bone(FILE *fp)
{
	vmdBoneKeyframe kf;

	read_bytes(kf.bone_name, fp, 15);

	read_bytes(&kf.frame_number, fp, 4);
	
	read_bytes(&kf.pos_x, fp, 4);
	read_bytes(&kf.pos_y, fp, 4);
	read_bytes(&kf.pos_z, fp, 4);

	read_bytes(&kf.rot_x, fp, 4);
	read_bytes(&kf.rot_y, fp, 4);
	read_bytes(&kf.rot_z, fp, 4);
	read_bytes(&kf.rot_w, fp, 4);

	read_bytes(kf.interpolation_data, fp, 64);

	return kf;
}

vmdFaceKeyframe vmd_read_face(FILE *fp)
{
	vmdFaceKeyframe kf;

	read_bytes(kf.face_name, fp, 15);

	read_bytes(&kf.frame_number, fp, 4);
	read_bytes(&kf.weight, fp, 4);

	return kf;
}

int vmd_read_header(FILE *fp)
{
	char ch[30];
	int i;

	for (i = 0; i < 30; ++i)
		ch[i] = fgetc(fp);

	printf("Header: %s\n", ch);
	return (strncmp(ch, "Vocaloid Motion Data file", 30) == 0 ? 1 : strncmp(ch, "Vocaloid Motion Data 0002", 30) == 0 ? 2 : 0);
}

void vmd_model_name(const int type, char * model, FILE *fp)
{
	int i;
	char * name;

	name = malloc(sizeof(char[(type * 10) + 1]));

	for (i = 0; i < (type * 10); ++i)
		name[i] = fgetc(fp);

	name[i] = '\0';

	memcpy(model, name, type * 10);

	free(name);
}
