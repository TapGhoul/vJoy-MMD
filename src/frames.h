#pragma once

#include "interpolation.h"

/* Bone info */
typedef struct _bone_data_ {
	char name[20];
	char english_name[20];
	uint32_t curr_frame, next_frame;
	size_t entries, size, index;
	bone_key_t *frames;
} bone_data;

/* Bone storage */
typedef struct _bone_store_ {
	char model_name[20];
	char english_name[20];
	char model_comment[256];
	uint16_t entries, size;
	bone_data **bones;
} bone_store;

/* Bone frame map */
typedef struct _bone_map_t_ {
	bone_axis_t axis;
	bone_data * bone;
} bone_map_t;

/* Create a bone store */
bone_store *bs_create(const char *name,
	const char *comment, const uint16_t size);

/* Free a bone store */
void bs_free(bone_store *store);

/* Get a bone */
bone_data *bs_get(bone_store *store, const char *name);

/* Create a new bone */
bone_data *bd_create(const char *name, const char *english_name);

/* Add a bone to the store */
void bs_add(bone_store *store, bone_data *bone);

/* Add a frame to a bone */
void bd_add(bone_data *bone, bone_key_t frame);
