#include "pmd_parser.h"
#include <stdlib.h>
#include <errno.h>

float pmd_read_header(FILE *fp)
{
	const char valid_tag[3] = {'P', 'm', 'd'};
	float version;
	int i;

	for (i = 0; i < 3; ++i)
		if (fgetc(fp) != valid_tag[i])
			return 0.0f;

	read_bytes(&version, fp, 4);

	return version;
}

void pmd_model_name(FILE *fp, char *model_name, char *model_comment)
{
	read_bytes(model_name, fp, 20);
	read_bytes(model_comment, fp, 256);

	// printf("Name: %s\n", model_name);
	// printf("Comment: %s\n", model_comment);

}

pmd_bone_t pmd_read_bone(FILE *fp)
{
	pmd_bone_t pb;

	read_bytes(pb.bone_name, fp, 20);
	read_bytes(&pb.parent_index, fp, 2);
	read_bytes(&pb.child_index, fp, 2);
	read_bytes(&pb.bone_type, fp, 1);
	read_bytes(&pb.target_bone, fp, 2);

	read_bytes(&pb.x_pos, fp, 4);
	read_bytes(&pb.y_pos, fp, 4);
	read_bytes(&pb.z_pos, fp, 4);

	// printf("Type: %hu\n", pb.parent_index);

	return pb;
}

void pmd_read_ik(FILE *fp)
{
	uint8_t link_count;

	fseek(fp, 4, SEEK_CUR);
	read_bytes(&link_count, fp, 1);
	fseek(fp, 6, SEEK_CUR);
	fseek(fp, 2 * link_count, SEEK_CUR);
}

pmd_face_morph_t pmd_read_face_morph(FILE *fp)
{
	pmd_face_morph_t fm;

	read_bytes(fm.morph_name, fp, 20);
	read_bytes(&fm.vertices, fp, 4);
	read_bytes(&fm.face_type, fp, 1);

	fseek(fp, 16 * fm.vertices, SEEK_CUR);

	return fm;	
}

bone_store *pmd_read(const char *filename)
{
	FILE *fp;
	char model_name[20], model_comment[256];
	float version;
	uint8_t char_byte_count, bone_group_count;
	uint16_t short_byte_count, bone_count, face_morph_count;
	uint32_t byte_count;
	char bone_name[20], face_morph_name[20], bone_group_name[50];
	pmd_bone_t *bones;
	pmd_face_morph_t *morphs;
	bone_store *store;
	bone_data *bone;
	size_t i;
	int errsv;


	/* Try to open the file */
	fp = fopen(filename, "rb");
	if (fp == NULL) {
		errsv = errno;
		perror("Error opening PMD file");
		getchar();
		exit(errsv);
	}

	version = pmd_read_header(fp);
	if (version == 0.0f) {
		fprintf(stderr, "PMD file is not valid\n");
		fclose(fp);
		getchar();
		exit(EINVAL);
	} else {
		// printf("PMD version: %f\n", version);
	}

	/* Read the model name */
	pmd_model_name(fp, model_name, model_comment);

	/* Skip vertex info */
	read_bytes(&byte_count, fp, 4);
	// printf("Vertex count: %u\n", byte_count);
	fseek(fp, 38 * byte_count, SEEK_CUR);

	/* Skip face info */
	read_bytes(&byte_count, fp, 4);
	// printf("Face count: %u\n", byte_count);
	fseek(fp, 2 * byte_count, SEEK_CUR);

	/* Skip material info */
	read_bytes(&byte_count, fp, 4);
	// printf("Material count: %u\n", byte_count);
	fseek(fp, 70 * byte_count, SEEK_CUR);

	/* Bones info */
	read_bytes(&bone_count, fp, 2);
	// printf("Bone count: %u\n", bone_count);
	bones = malloc(sizeof(pmd_bone_t) * bone_count);

	store = bs_create(model_name, model_comment, bone_count);

	for (i = 0; i < bone_count; ++i) {
		bones[i] = pmd_read_bone(fp);
		// printf("Bone: %s (%f %f %f)\n",
		// 	bones[i].bone_name,
		// 	bones[i].x_pos, bones[i].y_pos, bones[i].z_pos);
	}

	/* Skip IK info */
	read_bytes(&short_byte_count, fp, 2);
	// printf("IK count: %u\n", short_byte_count);
	for (i = 0; i < short_byte_count; ++i)
		pmd_read_ik(fp);

	/* Face morph info */
	read_bytes(&face_morph_count, fp, 2);
	// printf("Face morph count: %u\n", face_morph_count);

	morphs = malloc(sizeof(pmd_face_morph_t) * face_morph_count);

	for (i = 0; i < face_morph_count; ++i) {
		morphs[i] = pmd_read_face_morph(fp);
		// printf("Morph: %s: %u %u\n",
		// 	morphs[i].morph_name,
		// 	morphs[i].vertices, morphs[i].face_type);
	}

	/* Skip face display info */
	read_bytes(&char_byte_count, fp, 1);
	// printf("Faces to display: %u\n", char_byte_count);
	fseek(fp, 2 * char_byte_count, SEEK_CUR);

	/* Skip bone group name info */
	read_bytes(&bone_group_count, fp, 1);
	// printf("Bone group count: %u\n", bone_group_count);
	fseek(fp, 50 * bone_group_count, SEEK_CUR);

	/* Skip bones grouping info */
	read_bytes(&byte_count, fp, 4);
	// printf("Bones to display: %u\n", byte_count);
	fseek(fp, 3 * byte_count, SEEK_CUR);

	// End of file (no english data)?
	if (feof(fp) != 0) {
		for (i = 0; i < bone_count; ++i) {
			bone = bd_create(bones[i].bone_name, NULL);
			bs_add(store, bone);
			//ht_add(table, bones[i].bone_name, '\0');
		}
		printf("No English data!\n");
		fclose(fp);
		free(bones);
		free(morphs);
		return store;
	}

	/* Check if english data */
	read_bytes(&char_byte_count, fp, 1);
	if (char_byte_count != 1) {
		for (i = 0; i < bone_count; ++i) {
			bone = bd_create(bones[i].bone_name, NULL);
			bs_add(store, bone);
			//ht_add(table, bones[i].bone_name, '\0');
		}
		printf("No English data!\n");
		fclose(fp);
		free(bones);
		free(morphs);
		return store;
	} else {
		printf("We have English data!\n");
	}

	/* English name */
	pmd_model_name(fp, store->english_name, store->model_comment);

	//memcpy(store->english_name, model_name, 20);
	//memcpy(store->model_comment, model_comment, 256);

	/* English bone names */
	for (i = 0; i < bone_count; ++i) {
		read_bytes(bone_name, fp, 20);
		memcpy(bones[i].english_bone_name, bone_name, 20);
		bone = bd_create(bones[i].bone_name, bones[i].english_bone_name);
		bs_add(store, bone);
		//ht_add(table, bones[i].bone_name, bones[i].english_bone_name);
		// printf("Bone name %s: %s\n", bones[i].bone_name, bones[i].english_bone_name);
	}

	/* English face morph names */
	for (i = 0; i < face_morph_count; ++i) {
		read_bytes(face_morph_name, fp, 20);
		memcpy(morphs[i].english_morph_name, face_morph_name, 20);
		// printf("Face morph name %s: %s\n", morphs[i].morph_name, morphs[i].english_morph_name);
	}

	/* English bone group names */
	for (i = 0; i < bone_group_count; ++i) {
		read_bytes(bone_group_name, fp, 50);
		// printf("Bone group name: %s\n", bone_group_name);
	}

	/* Close the file */
	fclose(fp);
	free(bones);
	free(morphs);

	return store;
}
