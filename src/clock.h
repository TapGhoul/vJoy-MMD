#pragma once

#include <time.h>
#include <stdint.h>

uint32_t elapsed_frames(const struct timespec t_start);
