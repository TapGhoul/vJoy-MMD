#pragma once

#include <stdint.h>
#include "vmd_types.h"

/* Read a bone keyframe from a VMD file */
vmdBoneKeyframe vmd_read_bone(FILE *fp);

/* Read a face keyframe from a VMD file */
vmdFaceKeyframe vmd_read_face(FILE *fp);

/* Read the VMD header */
int vmd_read_header(FILE *fp);

/* Read the model name from a VMD file */
void vmd_model_name(const int type, char *model, FILE *fp);
