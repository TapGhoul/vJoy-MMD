#include "vjoy.h"
#include <stdio.h>
#include <windows.h>
#include "../include/vjoy/public.h"
#include "../include/vjoy/vjoyinterface.h"

int vjoyInit()
{
	WORD VerDll, VerDrv;
	enum VjdStat status;
	unsigned int iInterface;
	BOOL AxisX, AxisY, AxisZ, AxisRX, AxisRY, AxisRZ, AxisSL0, AxisSL1;
	BOOL joy_available = FALSE;
	int nButtons;

	if (!vJoyEnabled()) {
		fprintf(stderr, "vJoy is not enabled!\n");
		getchar();
		exit(2);
	} else {
		printf(">> Vendor: %S\n>> Product: %S\n>> Version: %S\n",
			GetvJoyManufacturerString(),
			GetvJoyProductString(),
			GetvJoySerialNumberString());
	}

	if (!DriverMatch(&VerDll, &VerDrv)) {
		fprintf(stderr, "Error: vJoy driver version %04x does not match vJoyInterface DLL %04x!\n",
			VerDrv, VerDll);
		getchar();
		exit(3);
	} else {
		printf("vJoy driver and vJoyInterface DLL versions match! (%04x)\n",
			VerDrv);
	}

	for (iInterface = 1; iInterface <= 16; ++iInterface) {
		status = GetVJDStatus(iInterface);
		switch (status) {
			case VJD_STAT_OWN:
				joy_available = TRUE;
				break;
			case VJD_STAT_FREE:
				printf("vJoy device %d is free\n", iInterface);
				if (AcquireVJD(iInterface))
					joy_available = TRUE;
				break;
			default:
				break;
		}
		if (joy_available)
			break;
	}

	if (!joy_available) {
		fprintf(stderr, "Error: No vJoy device available!\n");
		getchar();
		exit(4);
	}

	AxisX = GetVJDAxisExist(iInterface, HID_USAGE_X);
	AxisY = GetVJDAxisExist(iInterface, HID_USAGE_Y);
	AxisZ = GetVJDAxisExist(iInterface, HID_USAGE_Z);
	AxisRX = GetVJDAxisExist(iInterface, HID_USAGE_RX);
	AxisRY = GetVJDAxisExist(iInterface, HID_USAGE_RY);
	AxisRZ = GetVJDAxisExist(iInterface, HID_USAGE_RZ);
	AxisSL0 = GetVJDAxisExist(iInterface, HID_USAGE_SL0);
	AxisSL1 = GetVJDAxisExist(iInterface, HID_USAGE_SL1);
	nButtons = GetVJDButtonNumber(iInterface);

	printf("\nvJoy device %d capabilities:\n", iInterface);
	printf("\tButtons:\t%d\n", nButtons);
	printf("\tAxis X: \t%s\n", AxisX ? "Yes" : "No");
	printf("\tAxis Y: \t%s\n", AxisY ? "Yes" : "No");
	printf("\tAxis Z: \t%s\n", AxisZ ? "Yes" : "No");
	printf("\tAxis RX:\t%s\n", AxisRX ? "Yes" : "No");
	printf("\tAxis RY:\t%s\n", AxisRY ? "Yes" : "No");
	printf("\tAxis RZ:\t%s\n", AxisRZ ? "Yes" : "No");
	printf("\tAxis SL0:\t%s\n", AxisSL0 ? "Yes" : "No");
	printf("\tAxis SL1:\t%s\n", AxisSL1 ? "Yes" : "No");

	ResetVJD(iInterface);

	return iInterface;
}

void vjoyCleanup(const int iInterface)
{
	RelinquishVJD(iInterface);
}

int interp(int index, int pos)
{
	int newPos = pos + (index << 12);
	newPos = newPos % 0x10000;
	if (newPos > 0x8000)
		newPos = 0x10000 - newPos;

	return newPos;
}

void vjoyWave(const int iInterface, const int scrollPos)
{
	JOYSTICK_POSITION iReport;

	iReport.bDevice = (BYTE) iInterface;
	iReport.wAxisX = interp(0, scrollPos);
	iReport.wAxisY = interp(1, scrollPos);
	iReport.wAxisZ = interp(2, scrollPos);
	iReport.wAxisXRot = interp(3, scrollPos);
	iReport.wAxisYRot = interp(4, scrollPos);
	iReport.wAxisZRot = interp(5, scrollPos);
	iReport.wSlider = interp(6, scrollPos);
	iReport.wDial = interp(7, scrollPos);

	iReport.lButtons = 1 << ((scrollPos/0x800) % 8);
	iReport.lButtons |= 1 << (7-((scrollPos/0x800) % 8));

	UpdateVJD(iInterface, &iReport);
}

void vjoySet(const int iInterface, const vjoy_state_t state)
{
	JOYSTICK_POSITION iReport;
	size_t i;

	iReport.bDevice = (BYTE) iInterface;
	iReport.wAxisX = state.axis[0];
	iReport.wAxisY = state.axis[1];
	iReport.wAxisZ = state.axis[2];
	iReport.wAxisXRot = state.axis[3];
	iReport.wAxisYRot = state.axis[4];
	iReport.wAxisZRot = state.axis[5];
	iReport.wSlider = state.axis[6];
	iReport.wDial = state.axis[7];

	iReport.lButtons = 0;
	for (i = 0; i < 8; ++i) {
		if (state.buttons[i])
			iReport.lButtons |= 1 << i;
	}

	UpdateVJD(iInterface, &iReport);
}
